#-------------------------------------------------------
# Utility functions script
#-------------------------------------------------------

extends Node

#-------------------------------------------------------
# Files related
#-------------------------------------------------------

func get_file_contents(path):
	"""
	Reads the contents of a file
	"""
	var file = File.new()
	var buffer = null
	if file.open(path, file.READ) == 0:
		buffer = file.get_as_text()
		file.close()
	return buffer


func set_file_contents(path, data):
	"""
	Saves given data into a file
	"""
	var file = File.new()
	file.open(path, file.WRITE)
	file.store_string(data)
	file.close()


func load_json_file(path):
	var content = get_file_contents(path)
	if content != null:
		return parse_json_data(content)
	return null


#-------------------------------------------------------
# Data serialization related
#-------------------------------------------------------

func parse_json_data(json):
	var result = JSON.parse(json)
	if result.error == OK:
		return result.result
	return null


func serialize_to_gdscript(data):
	"""
	Serializes data to the format it would be written in GDScript
	"""
	var value
	match typeof(data):
		TYPE_STRING:
			value = "\"%s\"" % data
		TYPE_BOOL:
			value = String(data).to_lower()
		TYPE_ARRAY, TYPE_STRING_ARRAY:
			value = "["
			for item in data:
				value += "%s, " % serialize_to_gdscript(item)
			value += "]"
		TYPE_DICTIONARY:
			value = "{"
			for key in data.keys():
				value += "\"%s\": %s, " % [key, serialize_to_gdscript(data[key])]
			value += "}"
		_:
			if data == null:
				value = "null"
			else:
				value = String(data)
	return value


#-------------------------------------------------------
# Regular expression related
#-------------------------------------------------------

func setup_regex(pattern):
	var re = RegEx.new()
	re.compile(pattern)
	return re


#-------------------------------------------------------
# Audio related
#-------------------------------------------------------

func percent_to_db(percent):
	return 20 * (log(percent/100.0)/log(10))


func db_to_percent(dB):
	return 100 * pow(10, dB/20.0)


#-------------------------------------------------------
# GDScript data evaluation related
#-------------------------------------------------------

func get_script_object(sources):
	"""
	Returns an object with an script loaded from given sources
	"""
	var script = GDScript.new()
	script.set_source_code(sources)
	script.reload()
	var obj = Reference.new()
	obj.set_script(script)
	return obj


#-------------------------------------------------------
# Array utilities
#-------------------------------------------------------

func array_slice(array, from, to):
	var sliced = []
	for i in range(from, to + 1):
		sliced.append(array[i])
	return sliced


func array_join(array, separator=' '):
	"""
	Joins values in an Array into a single string
	"""
	var joined_string = ''
	var index = 0
	var size = array.size()
	for elem in array:
		index += 1
		joined_string += str(elem)
		if index < size:
			joined_string += separator
	return joined_string

#-------------------------------------------------------
# Date and time related utilities
#-------------------------------------------------------

func slugify(text):
	"""
	Returns slugified version of given text
	"""
	return text.to_lower().replace(' ', '_')

#-------------------------------------------------------
# Date and time related utilities
#-------------------------------------------------------

func get_timestamp(date=true, time=true):
	"""
	Returns a timestamp string
	"""
	var datestring = ""
	var timestring = ""
	var dt = OS.get_datetime()
	if date:
		datestring = "%s-%s-%s" % [dt["year"], dt["month"], dt["day"]]
	if time:
		timestring = "%s:%s:%s" % [dt["hour"], dt["minute"], dt["second"]]
	var timestamp = "%s %s" % [datestring, timestring]
	return timestamp.strip_edges()
	
	