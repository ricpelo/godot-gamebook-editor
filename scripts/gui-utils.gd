extends Node

func show_dialog(dialog, parent=null, modal=true, centered=true):
	# Add dialog to parent
	if parent:
		parent.add_child(dialog)
	# Center in parent
	if centered:
		dialog.rect_position = dialog.get_parent().rect_size / 2 - dialog.rect_size / 2
		# dialog.set_anchors_and_margins_preset(Control.PRESET_CENTER)
	# Show the dialog
	if modal:
		dialog.show_modal()
	else:
		dialog.show()
	return dialog

func show_information(text, parent, confirm_callback=null, callback_target=null):
	"""
	Shows a confirmation dialog and connects its signals in only one call
	"""
	var dialog = AcceptDialog.new()
	dialog.set_text(text)
	# Bind confirmed callback
	if confirm_callback:
		if not callback_target:
			callback_target = parent
		dialog.connect("confirmed", callback_target, confirm_callback)
	# Remove and free the dialog if it hides
	dialog.connect("hide", dialog, "queue_free")
	# Show dialog
	return show_dialog(dialog, parent)
	

func ask_for_confirmation(text, parent, confirm_callback=null, cancel_callback=null, callback_target=null):
	"""
	Shows a confirmation dialog and connects its signals in only one call
	"""
	var dialog = ConfirmationDialog.new()
	dialog.set_text(text)
	# Bind confirmed callback
	if not callback_target:
		callback_target = parent
	if confirm_callback:
		dialog.connect("confirmed", callback_target, confirm_callback)
	# Bind cancellation callback
	if cancel_callback:
		dialog.get_cancel().connect("pressed", callback_target, cancel_callback)
	# Remove and free the dialog if it hides
	dialog.connect("hide", dialog, "queue_free")
	# Show dialog
	return show_dialog(dialog, parent)


func ask_for_path(
		basedir, parent, mode='file', filters=null, 
		confirm_callback=null, cancel_callback=null, callback_target=null,
		size=Vector2(400, 400)):
	var dialog = FileDialog.new()
	if basedir.begins_with('res://'):
		dialog.access = FileDialog.ACCESS_RESOURCES
	elif basedir.begins_with('user://'):
		dialog.access = FileDialog.ACCESS_USERDATA
	else:
		dialog.access = FileDialog.ACCESS_FILESYSTEM
	# Set dialog mode
	if mode:
		if mode == 'files':
			dialog.mode = FileDialog.MODE_OPEN_FILES
		elif mode == 'dir':
			dialog.mode = FileDialog.MODE_OPEN_DIR
		elif mode == 'any':
			dialog.mode = FileDialog.MODE_OPEN_ANY
		elif mode == 'save':
			dialog.mode = FileDialog.MODE_SAVE_FILE
		else:
			dialog.mode = FileDialog.MODE_OPEN_FILE
	# Add filters
	if filters:
		for filter in filters:
			dialog.add_filter(filter)
	# Connect dialog signals
	if not callback_target:
		callback_target = parent
	if mode == 'dir':
		dialog.connect('dir_selected', callback_target, confirm_callback)
	elif mode == 'files':
		dialog.connect('files_selected', callback_target, confirm_callback)
	else:
		dialog.connect('file_selected', callback_target, confirm_callback)
	if cancel_callback:
		dialog.get_cancel().connect("pressed", callback_target, cancel_callback)
	# Set dialog size
	dialog.rect_size = size
	#  Set current dir
	dialog.current_dir = basedir
	# Show dialog
	show_dialog(dialog, parent)
	# Invalidate dialog to update file list
	dialog.invalidate()
	return dialog

func populate_menus(menu_structure, base_node, menu_pressed_callback=null):
	"""
	Tool function to populate menus
	"""
	for menu in menu_structure.keys():
		var popup = base_node.get_node(menu).get_popup()
		for item in menu_structure[menu]:
			if not item:
				popup.add_separator()
			# Check if item has more information
			elif typeof(item) == TYPE_ARRAY:
				# Check if item has an icon
				if typeof(item[0]) == TYPE_ARRAY:
					var text = item[0][0]
					var icon = load(item[0][1])
					if item.size() > 1 and item[1] != null:
						# Icon item with shortcut
						popup.add_icon_item(icon, text, -1, item[1])
					else:
						# Icon item without shortcut
						popup.add_icon_item(icon, text)
				else:
					if item.size() > 1 and item[1] != null:
						# Text item with shortcut
						popup.add_item(item[0], -1, item[1])
					else:
						# Text item without shortcut
						popup.add_item(item[0])
			else:
				# Simple text item
				popup.add_item(item)
		# Connect callback for pressed menu items
		if menu_pressed_callback:
			# Callback has been specified. We use base node and callback for menu pressed handling
			popup.connect("id_pressed", base_node, menu_pressed_callback, [base_node, menu_structure, menu])
		else:
			# Use builtin function handle_menu_pressed
			popup.connect("id_pressed", self, 'handle_menu_pressed', [base_node, menu_structure, menu])

func handle_menu_pressed(id, base_node, menu_structure, menu):
	var elem = menu_structure[menu][id]
	if typeof(elem) == TYPE_ARRAY:
		if elem.size() > 2:
			var fr = funcref(base_node, elem[2])
			if elem.size() > 3:
				fr.call_func(elem[3])
			else:
				fr.call_func()

func apply_syntax_highlighting_rules(textedit, rules):
	"""
	# Sample rules
	var rules = [
		['mykeyword', Color(1, 0, 0)], # Single keyword
		['"', '"', Color(1, 1, 0)], # Region
		['\'', '\'', Color(1, 1, 0), false], # Region
		['@', ' ', Color(1, 0, 1), true], # Line only region
	]
	"""
	for rule in rules:
		var size = rule.size()
		if size == 4:
			textedit.add_color_region(rule[0], rule[1], rule[2], rule[3])
		elif size == 3:
			textedit.add_color_region(rule[0], rule[1], rule[2])
		elif size == 2:
			textedit.add_keyword_color(rule[0], rule[1])
		else:
			# Invalid rule. We ignore it
			pass


# Itemlist utilities

func find_itemlist_item_by_text(itemlist, text):
	for index in range(itemlist.get_item_count()):
		if itemlist.get_item_text(index) == text:
			return index
	return null
