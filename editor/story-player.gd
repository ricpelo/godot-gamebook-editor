extends WindowDialog

onready var app = get_parent()

var section_history = []
var story_state = {}
var current_section = null

func _ready():
	var close_button = get_close_button()
	close_button.connect('pressed', self, 'stop')

func play(start_node):
	story_state = {}
	section_history = []
	$VBoxContainer/BackHBoxContainer/BackButton.disabled = true
	show_section(start_node)

func show_section(section):
	var current_module = null
	if current_section:
		current_module = app.find_module_for_section(current_section.name)
	var module = app.find_module_for_section(section.name)
	if current_module != module:
		app.switch_to_module(module)
	$VBoxContainer/HBoxContainer/Title.text = section.title
	$VBoxContainer/HBoxContainer/Label.text = section.label
	$VBoxContainer/HBoxContainer2/Tags.text = section.tags
	$VBoxContainer/RichTextLabel.text = section.content
	# Setup options
	setup_options(section)
	if current_section:
		var current_section_graphnode = app.section_graph.get_section(current_section.name)
		current_section_graphnode.overlay = GraphNode.OVERLAY_DISABLED
	current_section = section
	var section_graphnode = app.section_graph.get_section(section.name)
	section_graphnode.overlay = GraphNode.OVERLAY_BREAKPOINT
	section_history.append(current_section.name)
	if section_history.size() > 1:
		$VBoxContainer/BackHBoxContainer/BackButton.disabled = false
	
func setup_options(section):
	var options = $VBoxContainer/OptionsScroll/OptionsVBoxContainer
	# Clear previous options
	for child in options.get_children():
		options.remove_child(child)
		child.queue_free()
	# Populate with new options
	var section_graphnode = app.section_graph.get_section(section.name)
	for option in section_graphnode.get_children():
		var label = option.get_node('OptionText')
		if label:
			var button = Button.new()
			button.text = label.text
			button.connect('pressed', self, '_on_Option_pressed', [option.get_index() - section_graphnode.initial_child_count - 1])
			options.add_child(button)

func stop():
	if current_section:
		var current_section_graphnode = app.section_graph.get_section(current_section.name)
		current_section_graphnode.overlay = GraphNode.OVERLAY_DISABLED
		current_section.overlay = GraphNode.OVERLAY_DISABLED

func back():
	if section_history.size() > 1:
		section_history.erase(section_history[section_history.size() - 1])
		var section_name = section_history[section_history.size() - 1]
		section_history.erase(section_history[section_history.size() - 1])
		if section_history.size() == 0:
			$VBoxContainer/BackHBoxContainer/BackButton.disabled = true
		show_section(app.get_section(section_name))
		
	else:
		GUIUtils.show_information(tr('Can not rewind back the starting section.'), app)


# Events

func _on_Option_pressed(index):
	var section_graphnode = app.section_graph.get_section(current_section.name)
	var direct_connection = section_graphnode.get_option_by_index(index).direct_connection
	if direct_connection:
		show_section(app.get_section(direct_connection))
		return
	var connection = app.section_graph.find_connection(current_section.name, index)
	if connection:
		show_section(app.get_section(connection.to))
	else:
		GUIUtils.show_information(tr('This option is not connected or has a direct connection'), app)

func _on_StoryPlayerWindow_modal_closed():
	stop()

func _on_Button_pressed():
	back()
