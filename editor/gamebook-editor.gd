extends Panel

export(PackedScene) var section_scene

# Menu definitions
var MENUS = {
	'MainVBoxContainer/MenuHBoxContainer/FileMenu': [
		[tr('New'), KEY_MASK_CTRL + KEY_MASK_SHIFT + KEY_N, 'confirm_new_story'],
		null,
		[tr('Open'), KEY_MASK_CTRL + KEY_O, 'open_story'],
		# [tr('Open recent projects'), KEY_MASK_CTRL + KEY_MASK_SHIFT + KEY_O, 'open_recent_story'],
		null,
		[tr('Save'), KEY_MASK_CTRL + KEY_S, 'save_story'],
		[tr('Save as...'), KEY_MASK_CTRL + KEY_MASK_SHIFT + KEY_S, 'save_story_as'],
		null,
		[tr('Export as GBE file'), KEY_MASK_CTRL + KEY_E, 'export_gbe_file'],
		null,
		[tr('Quit'), KEY_MASK_CTRL + KEY_Q, 'confirm_quit']
	],
	'MainVBoxContainer/MenuHBoxContainer/ViewMenu': [
		[tr('Zoom in'), KEY_KP_SUBTRACT, 'zoom_out'],
		[tr('Zoom out'), KEY_KP_ADD, 'zoom_in'],
	],	
	'MainVBoxContainer/MenuHBoxContainer/StoryMenu': [
		[tr('Story configuration'), KEY_F12, 'story_config'],
		null,
		[tr('Check for errors'), KEY_F4, 'check_story'],
		null,
		[tr('Play from start'), KEY_F5, 'play_story'],
		[tr('Play from selected'), KEY_MASK_SHIFT + KEY_F5, 'play_story', [true]],
		null,
		[tr('Play from start on external player'), KEY_MASK_CTRL + KEY_F5, 'play_story_external'],
		[tr('Play from selected on external player'), KEY_MASK_CTRL + KEY_MASK_SHIFT + KEY_F5, 'play_story_external', [true]],
	],
	'MainVBoxContainer/MenuHBoxContainer/SectionMenu': [
		[tr('Add section'), KEY_MASK_CTRL + KEY_N, 'add_section'],
		null,
		[tr('Set as start section'), null, 'set_selected_as_start_section'],
		[tr('Edit section'), null, 'edit_selected_section'],
		[tr('Delete section'), KEY_MASK_CTRL + KEY_X, 'delete_selected_section']
	]
}

var DEFAULT_STORY_CONFIG = {
	'title': tr('New story'),
	'description': tr('Story description'),
	'author': tr('Story author'),
	'ext_project_path': '',
	'ext_story_path': ''
#	'ext_project_path': '/home/oliver/Revil/Devel/an-gbe-player/game',
#	'ext_story_path': '/home/oliver/Revil/Devel/an-gbe-player/game/story'
}

onready var section_graph = find_node('SectionGraph')
onready var status_bar = find_node('StatusBar')
onready var stats_bar = find_node('StatsBar')
onready var modules_list = find_node('ModulesList')
onready var BASE_FILE_PATH = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)

var CURRENT_FILE = null
var CURRENT_DIR = null
var CURRENT_EXPORT_DIR = null
var STORY_CONFIG = null
var MODULES_DATA = null
var CURRENT_MODULE = null


# ---------------------------------------------------------------------------------------------
# Internal functions
# ---------------------------------------------------------------------------------------------

func _ready():
	# Disable automatic quit
	get_tree().set_auto_accept_quit(false)
	
	# Populate menus
	GUIUtils.populate_menus(MENUS, self)
	
	# Initialize section graph
	section_graph.init(self)
	
	# Dialogs initialization
	$SectionSelection.init(self)
	$ExpressionEditor.init(self)
	
	# Start with a fresh new story
	new_story()

func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		# Skip frame for notification to allow confirm_quit to show dialog
		yield(get_tree(), 'idle_frame')
		confirm_quit()


# ---------------------------------------------------------------------------------------------
# GUI and app flow functions
# ---------------------------------------------------------------------------------------------

func push_status(text):
	status_bar.text = text
	yield(get_tree(), 'idle_frame')
	status_bar.get_node('Timer').start()

func update_stats():
	yield(get_tree(), 'idle_frame')
	var total_modules = MODULES_DATA.keys().size()
	var total_sections = 0
	for module in MODULES_DATA.keys():
		if module == CURRENT_MODULE:
			total_sections += section_graph.get_sections().keys().size()
		else:
			total_sections += MODULES_DATA[module]['nodes'].keys().size()
	stats_bar.text = 'Modules: %s Sections: %s' % [total_modules, total_sections]

func confirm_new_story():
	GUIUtils.ask_for_confirmation(
		tr('You will lose all unsaved progress. Do you really want to continue?'),
		self,
		'new_story')

func new_story():
	# Remove_all_modules
	modules_list.clear()
	# Reset module data
	MODULES_DATA = {}
	# Create main module
	var module = create_module('Main')
	CURRENT_MODULE = 'Main'
	# Set main module as current module
	switch_to_module('Main')
	# Create first section
	var section = section_graph.create_section('start', 'Start section')
	section.set_as_start()
	# Reset story config
	STORY_CONFIG = DEFAULT_STORY_CONFIG
	# Reset other variables
	CURRENT_FILE = null
	# Update stats
	update_stats()

func create_module(name, description='', notes=''):
	if not MODULES_DATA.has(name):
		modules_list.add_item(name)
		modules_list.sort_items_by_text()
		MODULES_DATA[name] = {
			'nodes': {},
			'connections': [],
			'description': description,
			'notes': notes,
		}
		# Update stats
		update_stats()
	else:
		GUIUtils.show_information(tr('Module %s already exists.') % name, self)

func delete_selected_module():
	var selected = modules_list.get_selected_items()
	if selected:
		var module = modules_list.get_item_text(selected[0])
		modules_list.remove_item(selected[0])
		section_graph.remove_all_sections()
		MODULES_DATA.erase(module)
	modules_list.sort_items_by_text()
	# Update stats
	update_stats()

func save_module(name, description, notes):
	MODULES_DATA[name]['description'] = description
	MODULES_DATA[name]['notes'] = notes

func rename_module(oldname, name, description='', notes=''):
	MODULES_DATA[name] = MODULES_DATA[oldname]
#	MODULES_DATA[name]['description'] = description
#	MODULES_DATA[name]['notes'] = notes
	if CURRENT_MODULE == oldname:
		CURRENT_MODULE = name
	MODULES_DATA.erase(oldname)
	var index = GUIUtils.find_itemlist_item_by_text(modules_list, oldname)
	modules_list.set_item_text(index, name)
	modules_list.sort_items_by_text()

func switch_to_module(name):
	if name != CURRENT_MODULE and MODULES_DATA.has(name):
		# Serialize graph data to current module
		update_current_module()
		# Clear graph
		section_graph.remove_all_sections()
		# Load graph data from serialized data
		section_graph.restore_serialized_graph_data(MODULES_DATA[name])
		CURRENT_MODULE = name

func update_current_module():
	var module_description = MODULES_DATA[CURRENT_MODULE].get('description', '')
	var module_notes = MODULES_DATA[CURRENT_MODULE].get('notes', '')
	MODULES_DATA[CURRENT_MODULE] = section_graph.serialize_graph_data()
	MODULES_DATA[CURRENT_MODULE]['description'] = module_description
	MODULES_DATA[CURRENT_MODULE]['notes'] = module_notes

func add_section(label='', title='', tags='', content='', metadata='', notes='', pos=Vector2(0, 0), size=Vector2(0, 0)):
	var section = section_graph.create_section(label, title, tags, content, metadata, notes, pos, size)
	# Center section in current editor view
	edit_section(section)
	# Update stats
	update_stats()
	return section

func edit_selected_section():
	var selected_section = section_graph.get_selected_section(true)
	if selected_section:
		edit_section(selected_section)

func edit_section(section):
	$SectionWindow.init(section)
	GUIUtils.show_dialog($SectionWindow)
	$SectionWindow/VBoxContainer/TitleHBoxContainer/LineEdit.grab_focus()

func delete_selected_section():
	var selected_section = section_graph.get_selected_section(true)
	if selected_section:
		selected_section.confirm_remove()

func set_start_section(label):
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			MODULES_DATA[module]['nodes'][node]['start'] = (node == label)

func get_start_section():
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			if MODULES_DATA[module]['nodes'][node]['start']:
				return MODULES_DATA[module]['nodes'][node]

func get_section(label):
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			if node == label:
				return MODULES_DATA[module]['nodes'][node]

func section_exists(label):
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			if node == label:
				return true
	return false

func rename_direct_connections(oldname, newname):
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			var optionindex = 0
			for option in MODULES_DATA[module]['nodes'][node]['options']:
				if option[1] == oldname:
					MODULES_DATA[module]['nodes'][node]['options'][optionindex][1] = newname
				optionindex += 1

func find_module_for_section(label):
	for module in MODULES_DATA.keys():
		for node in MODULES_DATA[module]['nodes'].keys():
			if label == node:
				return module

func confirm_quit():
	GUIUtils.ask_for_confirmation(
		tr('Do you really want to quit?'),
		self,
		'quit_application')

func quit_application():
	get_tree().quit()

func set_selected_as_start_section():
	var selected_section = section_graph.get_selected_section(true)
	if selected_section:
		selected_section.set_as_start()

func story_config():
	$StoryOptions.init(self)
	GUIUtils.show_dialog($StoryOptions)

func save_story_as():
	if CURRENT_DIR == null:
		CURRENT_DIR = BASE_FILE_PATH
	# Ask for a file path
	GUIUtils.ask_for_path(CURRENT_DIR, self, 'save', ['*.gbs;Gamebook Story file'], 'save_story')
	
func save_story(path=null):
	if path != null:
		CURRENT_FILE = path
	if CURRENT_FILE:
		push_status(tr('Saving story to file') + CURRENT_FILE)
		CURRENT_DIR = CURRENT_FILE.get_base_dir()
		var data = serialize_story()
		var json = JSON.print(data)
		Utils.set_file_contents(CURRENT_FILE, json)
		push_status(tr('Finished saving story to file') + CURRENT_FILE)
	else:
		save_story_as()

func open_story():
	if CURRENT_DIR == null:
		CURRENT_DIR = BASE_FILE_PATH
	GUIUtils.ask_for_path(CURRENT_DIR, self, 'file', ['*.gbs;Gamebook Story file'], 'load_story')

func load_story(path):
	push_status(tr('Loading story from file ') + path)
	CURRENT_FILE = path
	CURRENT_DIR = path.get_base_dir()
	section_graph.remove_all_sections()
	var data = Utils.load_json_file(path)
	restore_story(data)
	push_status(tr('Finished loading story from file') + path)
	update_stats()

func serialize_story():
	update_current_module()
	var data = {
		'modules': MODULES_DATA,
		'config': STORY_CONFIG
	}
	return data

func restore_story(data):
	STORY_CONFIG = data.get('config', DEFAULT_STORY_CONFIG)
	if data.has('modules'):
		MODULES_DATA = data['modules']
	else:
		MODULES_DATA = {
			'Main': {
				'nodes': data['nodes'],
				'connections': data['connections'],
				'description': tr('Main module'),
				'notes': '',
			}
		}
	modules_list.clear()
	var modules = MODULES_DATA.keys()
	if modules:
		for module in modules:
			modules_list.add_item(module)
		modules_list.sort_items_by_text()
		CURRENT_MODULE = modules_list.get_item_text(0)
		modules_list.select(0)
		section_graph.restore_serialized_graph_data(MODULES_DATA[CURRENT_MODULE])

func export_gbe_file():
	if CURRENT_EXPORT_DIR == null:
		CURRENT_EXPORT_DIR = BASE_FILE_PATH
	# Ask for a file path
	GUIUtils.ask_for_path(CURRENT_EXPORT_DIR, self, 'save', ['*.gbe;Gamebook Engine file'], 'save_gbe_file')

func save_gbe_file(path):
	CURRENT_EXPORT_DIR = path.get_base_dir()
	var data = export_gbe()
	Utils.set_file_contents(path, data)

func export_gbe():
	var gbe = '# Exported story\n\n'
	for module in MODULES_DATA.keys():
		var data = MODULES_DATA[module]
		for key in data['nodes'].keys():
			var section = data['nodes'][key]
			# Prepare section label
			var label = '@' + section['name']
			if section['start']:
				label += '*'
			if section['metadata']:
				var json_data = Utils.parse_json_data(section['metadata'])
				if json_data:
					label += ' ' + JSON.print(json_data)
				else:
					print('WARNING!: Section metadata for section %s is not valid' % section['name'])
			# Section options
			var section_options = ''
			var section_index = 0
			for option in section['options']:
				var option_destination = null
				var option_condition = null
				if typeof(option) != TYPE_ARRAY:
					option = [option, null, null]
				if option[1]:
					option_destination = option[1]
				else:
					for connection in data['connections']:
						if connection[0] == section['name'] and connection[1] == section_index:
							option_destination = connection[2]
							if data['nodes'][option_destination]['label']:
								option_destination = data['nodes'][option_destination]['label']
							break
				if option.size() > 2 and option[2]:
					option_condition = option[2]
				var section_option = ''
				if option_destination:
					section_option = '[[' + option[0] + '|@' + option_destination + ']]\n'
					if option_condition:
						section_option = '?(%s) %s' % [option_condition, section_option]
				section_options += section_option
				section_index += 1
			# Compose section
			var section_fragment = ''
			section_fragment += label + '\n'
			section_fragment += '# %s\n' % section['title']
			section_fragment += section['content'] + '\n'
			section_fragment += section_options + '\n\n'
			gbe += section_fragment
	return gbe

func check_story():
	# Serialize current module graph
	update_current_module()
	return true
#	var start = null
#	var not_connected_nodes = []
#	var not_connected_opts = []
#
#	# Update data from current graph
#	MODULES_DATA[CURRENT_MODULE] = section_graph.serialize_graph_data()
#
#	for module in MODULES_DATA.keys():
#		var nodes = MODULES_DATA[module]['nodes']
#		var connections = MODULES_DATA[module]['connections']
#		for nodename in nodes.keys():
#			var node = nodes[nodename]
#			# Check for start node
#			if node['start']:
#				start = nodename
#			# Check if no input connections
#			if start != nodename:
#				var inbound_connected = false
#				for connection in connections:
#					if connection[2] == nodename:
#						inbound_connected = true
#						break
#				if not inbound_connected:
#					not_connected_nodes.append(nodename)
#					if module == CURRENT_MODULE:
#							node.overlay = node.OVERLAY_POSITION
#							node.hint_tooltip = 'This section is not connected to the story'
#						pass
			# TODO: Check for options without connections
#			var option_index = 0
#			for option in node['options']:
#				option_index += 1
#				var connected = false
#				for connection in connections:
#					if connection[0] == nodename:
#						if connection[1] == option_index:
#							connected = true
#							break
#				if not connected:
#					not_connected_opts.append([nodename, option])
#					if module == CURRENT_MODULE:
##							node.overlay = node.OVERLAY_POSITION
##							node.hint_tooltip = 'This section has options that are not connected to any other section'
#						pass
#	if start and not not_connected_nodes and not not_connected_opts:
#		GUIUtils.show_information(tr('No errors detected in the story.'), self)
#	else:
#		GUIUtils.show_information(tr('There are errors in the story.'), self)

func play_story(from_selected=false):
	if check_story():
		var start = null
		if from_selected:
			start = section_graph.get_selected_section(true)
			if not start:
				return
		else:
			start = get_start_section()
			if not start:
				GUIUtils.show_information(tr('There is no start section. You must define where the story begins.'), self)
				return
		$StoryPlayerWindow.play(start)
		GUIUtils.show_dialog($StoryPlayerWindow)
	else:
		GUIUtils.show_information(tr('There are errors in the story. Please check them before playing it.'), self)

func play_story_external(from_selected=false):
	# Check external configuration is set
	if not STORY_CONFIG['ext_story_path'] or not STORY_CONFIG['ext_project_path']:
		GUIUtils.show_information(tr('You have to configure the external player settings at story options.'), self)
		return
	if check_story():
		var start = null
		if from_selected:
			var selected = section_graph.get_selected_section(true)
			if selected:
				start = selected.label
			else:
				return
		else:
			start = get_start_section()
			if start:
				start = start.label
			else:
				GUIUtils.show_information(tr('There is no start section. You must define where the story begins.'), self)
				return
			
		# Export story as GBE file
		save_gbe_file(STORY_CONFIG['ext_story_path'] + '/main.gbe')
		# Excecute external project
		execute_project_player(STORY_CONFIG['ext_project_path'], start)
	else:
		GUIUtils.show_information(tr('There are errors in the story. Please check them before playing it.'), self)

func execute_project_player(project_path, start_section=null):
	push_status(tr('Executing external player at %s starting from %s') % [project_path, start_section])
	var vars = 'GBE_DEBUG=true'
	if start_section:
		vars += ' GBE_START_SECTION="@%s"' % start_section
	var command = 'cd %s && %s godot' % [project_path, vars]
	var output = []
	OS.execute('bash', ['-c', command], true, [])
	print(output)

func update_section_info(node):
	# Update modules data
	MODULES_DATA[CURRENT_MODULE] = section_graph.serialize_graph_data()
	# Update section info
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Label.text = '@' + node.label
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Title.text = node.title
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Tags.text = node.tags
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Content.bbcode_text = node.content
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Metadata.bbcode_text = node.metadata
	$MainVBoxContainer/StoryEditorVBoxContainer/EditorHBoxContainer/ScrollContainer/SectionInfo/Notes.bbcode_text = node.notes
func select_section(func_object, func_name):
	var callback = funcref(func_object, func_name)
	MODULES_DATA[CURRENT_MODULE] = section_graph.serialize_graph_data()
	$SectionSelection.update_data(callback)
	GUIUtils.show_dialog($SectionSelection)

func ask_for_expression(func_object, func_name):
	var callback = funcref(func_object, func_name)
	$ExpressionEditor.update_data(callback, func_object.condition_expression)
	GUIUtils.show_dialog($ExpressionEditor)

# ---------------------------------------------------------------------------------------------
# Events
# ---------------------------------------------------------------------------------------------

func _on_ModulesList_item_selected(index):
	# Get module name
	var module = modules_list.get_item_text(index)
	# Switch to new module
	switch_to_module(module)

func _on_NewModuleButton_pressed():
	$ModuleWindow.init(null)
	GUIUtils.show_dialog($ModuleWindow)

func _on_EditModuleButton_pressed():
	# Check selected module
	var selected = modules_list.get_selected_items()
	if selected:
		var module = modules_list.get_item_text(selected[0])
		$ModuleWindow.init(module)
		GUIUtils.show_dialog($ModuleWindow)
	else:
		GUIUtils.show_information(tr('There is no selected module'), self)

func _on_RemoveModuleButton_pressed():
	var selected = modules_list.get_selected_items()
	if selected:
		var module = modules_list.get_item_text(selected[0])
		GUIUtils.ask_for_confirmation(
			tr('Are you sure you want to remove %s module?' % module),
			self,
			'delete_selected_module')
	else:
		GUIUtils.show_information(tr('There is no selected module'), self)

func _on_StatusBarTimer_timeout():
	status_bar.text = ''
