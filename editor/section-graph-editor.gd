extends VBoxContainer

export(PackedScene) var section_scene

export(Color) var DEFAULT_NODE_COLOR

var app = null
var prev_scroll = null

onready var start_button_group = ButtonGroup.new()

func init(app_instance):
	app = app_instance

func zoom_out(factor=0.1):
	$GraphEdit.zoom -= factor

func zoom_in(factor=0.1):
	$GraphEdit.zoom += factor

func calculate_section_offset_center(section):
	var offset = ($GraphEdit.rect_size / 2) - (section.rect_size / 2)
	return offset + $GraphEdit.scroll_offset

func serialize_graph_data():
	"""
	Serializes data to simple JSON serializable datatypes
	"""
	# Serialize nodes
	var nodes = {}
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			nodes[child.name] = child.serialize()
	# Serialize connections
	var connections = []
	for connection in $GraphEdit.get_connection_list():
		connections.append([
			connection.from,
			connection.from_port,
			connection.to,
			connection.to_port])
	return {
		'nodes': nodes,
		'connections': connections
	}

func restore_serialized_graph_data(data):
	"""
	Restores graph from serialized data
	"""
	for key in data['nodes'].keys():
		var node_data = data['nodes'][key]
		# Create section node
		var section = create_section(
			node_data['label'],
			node_data['title'],
			node_data['tags'],
			node_data['content'],
			node_data['metadata'],
			node_data.get('notes', ''),
			Vector2(node_data['position'][0], node_data['position'][1]),
			Vector2(node_data['size'][0], node_data['size'][1]),
			node_data.get('color', DEFAULT_NODE_COLOR))
		# Set section node name
		section.name = node_data['name']
		# Add section node to editor
		if node_data['start']:
			section.set_as_start()
		for option in node_data['options']:
			if typeof(option) != TYPE_ARRAY:
				option = [option, null, null]
			var opt = section.add_option(option[0])
			if option[1]:
				opt.set_direct_connection(option[1])
			if option.size() > 2 and option[2]:
				opt.set_condition_expression(option[2])

	for connection in data['connections']:
		$GraphEdit.connect_node(
			connection[0].replace('@', ''),
			connection[1],
			connection[2].replace('@', ''),
			connection[3])

# ---------------------------------------------------------------------------------------------
# Node handling functions
# ---------------------------------------------------------------------------------------------

func create_section(label='', title='', tags='', content='', metadata='', notes='', pos=Vector2(0, 0), size=Vector2(0, 0), color=DEFAULT_NODE_COLOR):
	var section = section_scene.instance()
	$GraphEdit.add_child(section)
	section.init(self, label, title, tags, content, metadata, notes, pos, size, color)
	if pos == Vector2(0, 0):
		section.offset = calculate_section_offset_center(section)
	return section


func get_section(label):
	return $GraphEdit.get_node(label)

func get_sections():
	var sections = {}
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			sections[child.name] = child
	return sections

func get_start_section():
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			if child.is_start_section():
				return child
	# This should never happen
	return null

func get_selected_section(alert_if_no_selection=false):
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			if child.selected:
				return child
	if alert_if_no_selection:
		GUIUtils.show_information(
			tr('There is not section selected'), self)
	return null

func remove_all_sections():
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			child.remove()

func remove_connections(elem, from_slot=null):
	for connection in $GraphEdit.get_connection_list():
		if connection.from == elem:
			if from_slot != null:
				if connection.from_port == from_slot:
					$GraphEdit.disconnect_node(connection.from, connection.from_port, connection.to, connection.to_port)
					return
			else:
				$GraphEdit.disconnect_node(connection.from, connection.from_port, connection.to, connection.to_port)
		elif connection.to == elem:
			if from_slot == null:
				$GraphEdit.disconnect_node(connection.from, connection.from_port, connection.to, connection.to_port)

func rename_direct_connections(oldname, newname):
	for child in $GraphEdit.get_children():
		if child.get_class() == 'GraphNode':
			for option in child.get_children():
				if option.get('direct_connection') == oldname:
					option.set_direct_connection(newname)
	# Rename direct connections in other modules
	app.rename_direct_connections(oldname, newname)

func remake_connections(oldname, newname):
	if oldname != newname:
		# Remake graph connections
		for connection in $GraphEdit.get_connection_list():
			var from = connection.from
			var from_port = connection.from_port
			var to = connection.to
			var to_port = connection.to_port
			if connection.from == oldname:
				from = newname
				$GraphEdit.disconnect_node(from, from_port, to, to_port)
				$GraphEdit.connect_node(from, from_port, to, to_port)
			if connection.to == oldname:
				to = newname
				$GraphEdit.disconnect_node(from, from_port, to, to_port)
				$GraphEdit.connect_node(from, from_port, to, to_port)
		# Rename direct connections if needed
		rename_direct_connections(oldname, newname)

func reorder_connections(elem, from_slot):
	for connection in $GraphEdit.get_connection_list():
		if connection.from == elem:
			if connection.from_port > from_slot:
				$GraphEdit.disconnect_node(connection.from, connection.from_port, connection.to, connection.to_port)
				$GraphEdit.connect_node(connection.from, connection.from_port - 1, connection.to, connection.to_port)

func find_connection(from, from_slot, to=null, to_slot=null):
	for connection in $GraphEdit.get_connection_list():
		if connection.from == from:
			if connection.from_port == from_slot:
				return connection

#func check_story():
#	var start = null
#	var connections = $GraphEdit.get_connection_list()
#	var not_connected_nodes = []
#	var not_connected_opts = []
#	for child in $GraphEdit.get_children():
#		if child.get_class() == 'GraphNode':
#			var node = child
#			# Clear errors
#			node.overlay = node.OVERLAY_DISABLED
#			node.hint_tooltip = ''
#			# Check for start node
#			if node.get_node('HBoxContainer/StartCheckBox').pressed:
#				start = node
#			# Check if no input connections
#			if start != node:
#				var inbound_connected = false
#				for connection in connections:
#					if connection.to == node.name:
#						inbound_connected = true
#						break
#				if not inbound_connected:
#					node.overlay = node.OVERLAY_POSITION
#					node.hint_tooltip = 'This section is not connected to the story'
#					not_connected_nodes.append(node.name)
#			# Check for options without connections
#			for nodechild in node.get_children():
#				var child_index = nodechild.get_index() - node.initial_child_count - 1
#				if child_index > 0:
#					var connected = false
#					for connection in connections:
#						if connection.from == node.name:
#							if connection.from_port == child_index:
#								connected = true
#								break
#					if not connected:
#						node.overlay = node.OVERLAY_POSITION
#						node.hint_tooltip = 'This section has options that are not connected to any other section'
#						not_connected_opts.append([node.name, child_index])
#	return start and not not_connected_nodes and not not_connected_opts


# ---------------------------------------------------------------------------------------------
# Events
# ---------------------------------------------------------------------------------------------

func _on_GraphEdit_connection_request(from, from_slot, to, to_slot):
	"""
	Connection request handling
	"""
	# Check if there is already a connection from the same node and slot and remove it
	remove_connections(from, from_slot)
	# Create requested connection
	$GraphEdit.connect_node(from, from_slot, to, to_slot)

func _on_GraphEdit_disconnection_request(from, from_slot, to, to_slot):
	$GraphEdit.disconnect_node(from, from_slot, to, to_slot)

func _on_NewSectionButton_pressed():
	app.add_section()

func _on_GraphEdit_gui_input(ev):
	prev_scroll = null
	# TODO: Zoom control - Avoid scrolling
	if ev.get_class() == 'InputEventMouseButton':
		if ev.control and ev.button_index in [BUTTON_WHEEL_DOWN, BUTTON_WHEEL_UP]:
			if ev.pressed:
				prev_scroll = $GraphEdit.scroll_offset
				if ev.button_index == BUTTON_WHEEL_DOWN:
					zoom_out()
				elif ev.button_index == BUTTON_WHEEL_UP:
					zoom_in()

func _on_GraphEdit_scroll_offset_changed(ofs):
	if prev_scroll != null:
		$GraphEdit.scroll_offset = prev_scroll


func _on_GraphEdit_node_selected(node):
	app.update_section_info(node)
