extends WindowDialog

onready var app = get_parent()

var current_module = null

func init(module):
	current_module = module
	if module:
		$VBoxContainer/HBoxName/LineEdit.text = module
		$VBoxContainer/HBoxDescription/TextEdit.text = app.MODULES_DATA[module].get('description', '')
		$VBoxContainer/HBoxNotes/TextEdit.text = app.MODULES_DATA[module].get('notes', '')
	else:
		$VBoxContainer/HBoxName/LineEdit.text = tr('New module')
		$VBoxContainer/HBoxDescription/TextEdit.text = ''
		$VBoxContainer/HBoxNotes/TextEdit.text = ''

func save():
	# Validate name
	var module_name = $VBoxContainer/HBoxName/LineEdit.text
	if not module_name:
		GUIUtils.show_information(tr('Module name can not be empty'), get_parent())
		return
	var description = $VBoxContainer/HBoxDescription/TextEdit.text
	var notes = $VBoxContainer/HBoxNotes/TextEdit.text
	if current_module:
		if module_name != current_module:
			# Rename module
			app.rename_module(current_module, module_name, description, notes)
		else:
			app.save_module(module_name, description, notes)
	else:
		# Create new module
		app.create_module(module_name, description, notes)
	hide()

# Events

func _on_CancelButton_pressed():
	hide()

func _on_SaveButton_pressed():
	save()
