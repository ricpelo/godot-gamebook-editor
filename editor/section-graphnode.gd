extends GraphNode

const SIDE_LEFT = true
const SIDE_RIGHT = false

export(PackedScene) var option_scene = null

export(Color, RGB) var DEFAULT_LEFT_COLOR = Color(.5, .5, .5)
export(Color, RGB) var DEFAULT_RIGHT_COLOR = Color(0, .5, .8)

export(String, MULTILINE) var content
export(String) var label
export(String) var tags = null
export(String, MULTILINE) var metadata = null
export(String, MULTILINE) var notes = ''

var graph = null

onready var initial_child_count = get_child_count()

func _ready():
	var frame = get_stylebox('frame').duplicate()
	add_stylebox_override('frame', frame)
	var selectedframe = get_stylebox('selectedframe').duplicate()
	add_stylebox_override('selectedframe', selectedframe)

func init(graph_instance,
			section_label, section_title, section_tags,
			section_content, section_metadata, section_notes,
			position, size, color):
	graph = graph_instance
	if not section_label:
		label = name.replace('@', '')
	else:
		label = section_label
	name = label
	title = section_title
	tags = section_tags
	content = section_content
	metadata = section_metadata
	notes = section_notes
	# Position and size
	set_offset(position)
	rect_size = size
	# Add the start checkbox to the same button group to automate radio behavior
	$HBoxContainer/StartCheckBox.set_button_group(graph.start_button_group)
	# Add left end
	add_section_label(label)
	# Set color
	$HBoxContainer/ColorPickerButton.color = color
	set_color(color)
	# Set tooltip with notes
	update_note_hint()

func update_note_hint(note_text=null):
	if not note_text:
		note_text = notes
	if note_text:
		$HBoxContainer/NoteIndicator.hint_tooltip = note_text
		$HBoxContainer/NoteIndicator.show()
	else:
		$HBoxContainer/NoteIndicator.hide()

func update_data(old_name):
	get_node('SectionLabel').text = label
	graph.remake_connections(old_name, label)
	graph.app.update_section_info(self)
	update_note_hint()

func add_slot(element, side=SIDE_LEFT):
	add_child(element)
	var index = get_child_count() - 1
	set_slot(index,
		side, 0, DEFAULT_LEFT_COLOR,
		not side, 0, DEFAULT_RIGHT_COLOR)
	return index

func add_section_label(section_label):
	var label = Label.new()
	label.name = 'SectionLabel'
	label.text = section_label
	var index = add_slot(label, SIDE_LEFT)

func add_option(text=null):
	var option = option_scene.instance()
	var index = add_slot(option, SIDE_RIGHT)
	if text != null:
		option.get_node('OptionText').text = text
	else:
		option.edit()
	return option

func get_option_by_index(index):
	return get_child(initial_child_count + 1 + index)

func is_start_section():
	return $HBoxContainer/StartCheckBox.pressed

func set_as_start():
	$HBoxContainer/StartCheckBox.pressed = true
	# Update start at story data
	graph.app.set_start_section(label)

func confirm_remove():
	GUIUtils.ask_for_confirmation(
		tr("Do you really want to delete this section?"),
		graph,
		"remove",
		null,
		self)

func remove():
	var parent = get_parent()
	graph.remove_connections(self.name)
	parent.remove_child(self)
	# Update stats
	graph.app.update_stats()
	queue_free()

func serialize():
	var options = []
	for child in get_children():
		var label = child.get_node('OptionText')
		if label:
			options.append([label.text, child.direct_connection, child.condition_expression])
	return {
		'start': $HBoxContainer/StartCheckBox.pressed,
		'name': name.replace('@', ''),
		'label': label,
		'title': title,
		'tags': tags,
		'content': content,
		'metadata': metadata,
		'notes': notes,
		'position': [offset[0], offset[1]],
		'size': [rect_size[0], rect_size[1]],
		'color': $HBoxContainer/ColorPickerButton.color.to_html(false),
		'options': options
	}

func set_color(html_color):
	var frame = get_stylebox('frame')
	frame.bg_color = Color(html_color)
	var selected_frame = get_stylebox('selectedframe')
	selected_frame.bg_color = Color(html_color)

# ---------------------------------------------------------------------------------------------
# Events
# ---------------------------------------------------------------------------------------------

func _on_AddOptionButton_pressed():
	add_option()

func _on_GraphNode_resize_request(new_minsize):
	var size = Vector2()
	size.x = max(new_minsize.x, rect_min_size.x)
	size.y = max(new_minsize.y, rect_min_size.y)
	rect_size = size

func _on_StorySection_close_request():
	confirm_remove()

func _on_EditSectionButton_pressed():
	graph.app.edit_section(self)


func _on_StartCheckBox_toggled(button_pressed):
	if button_pressed:
		set_as_start()


func _on_ColorPickerButton_color_changed(color):
	set_color(color.to_html(false))
