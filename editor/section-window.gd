extends WindowDialog

onready var app = get_parent()
onready var content_textedit = $VBoxContainer/ContentHBoxContainer/TextEdit
onready var metadata_textedit = $VBoxContainer/MetadataHBoxContainer/TextEdit

var current_section = null

var content_sh_rules = [
	['"', '"', Color(1, 1, 0)],
	['\'', '\'', Color(1, 1, 0)],
	['@', ' ', Color(1, 0, 1), true]
]

var metadata_sh_rules = [
	['{', '}', Color(1, 0, 0)],
	['"', '"', Color(1, 1, 0)],
	['\'', '\'', Color(1, 1, 0)],
]

func _ready():
	# Content syntax highlighting
	GUIUtils.apply_syntax_highlighting_rules(content_textedit, content_sh_rules)
	# Metadata syntax highlighting
	GUIUtils.apply_syntax_highlighting_rules(metadata_textedit, metadata_sh_rules)

func init(section):
	current_section = section
	$VBoxContainer/LabelHBoxContainer/LineEdit.text = section.label
	$VBoxContainer/TitleHBoxContainer/LineEdit.text = section.title
	$VBoxContainer/TagsHBoxContainer/LineEdit.text = section.tags
	$VBoxContainer/ContentHBoxContainer/TextEdit.text = section.content
	$VBoxContainer/MetadataHBoxContainer/TextEdit.text = section.metadata
	$VBoxContainer/NotesHBoxContainer/TextEdit.text = section.notes

func _on_Button_pressed():
	# Validate title
	var title = $VBoxContainer/TitleHBoxContainer/LineEdit.text
	if not title:
		GUIUtils.show_information(tr('Title can not be empty'), get_parent())
		return
	# Validate label
	var label = $VBoxContainer/LabelHBoxContainer/LineEdit.text
	if not label:
		GUIUtils.show_information(tr('Label can not be empty'), get_parent())
		return
	if not label.is_valid_identifier():
		GUIUtils.show_information(tr('Label %s is not valid' % label), get_parent())
		return
	if current_section.label != label and app.section_exists(label):
		GUIUtils.show_information(tr('Label %s already exists' % label), get_parent())
		return

	# Validate metadata
	var metadata = $VBoxContainer/MetadataHBoxContainer/TextEdit.text.strip_edges()
	if metadata != '':
		var json_data = Utils.parse_json_data(metadata)
		if not json_data:
			GUIUtils.show_information(tr('Metadata is not valid JSON'), get_parent())
			return
	var old_name = current_section.name
	current_section.name = label
	current_section.label = label
	current_section.title = $VBoxContainer/TitleHBoxContainer/LineEdit.text
	current_section.tags = $VBoxContainer/TagsHBoxContainer/LineEdit.text
	current_section.content = $VBoxContainer/ContentHBoxContainer/TextEdit.text
	current_section.metadata = metadata
	current_section.notes = $VBoxContainer/NotesHBoxContainer/TextEdit.text
	current_section.update_data(old_name)
	hide()