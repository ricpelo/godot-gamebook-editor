extends HBoxContainer

var MENU = {
	'MenuButton': [
		[['Edit', 'res://assets/icons/icon_pencil.svg'], null, 'edit'],
		null,
		[['Set direct connection', 'res://assets/icons/icon_direct_connection.svg'], null, 'select_direct_connection_section'],
		[['Remove direct connection', 'res://assets/icons/icon_unset_direct_connection.svg'], null, 'remove_direct_connection_confirm'],
		null,
		[['Set condition', 'res://assets/icons/icon_condition.svg'], null, 'show_set_condition_expression'],
		[['Unset condition', 'res://assets/icons/icon_unset_condition.svg'], null, 'unset_condition_expression_confirm'],
		null,
		[['Disconnect', 'res://assets/icons/icon_disconnect.svg'], null, 'disconnect_option'],
		null,
		[['Move up', 'res://assets/icons/icon_up.svg'], null, 'move_up'],
		[['Move down', 'res://assets/icons/icon_down.svg'], null, 'move_down'],
		null,
		[['Delete', 'res://assets/icons/icon_trash.svg'], null, 'delete_confirm']
	]
}

export(String) var direct_connection = null
export(String) var condition_expression = null

var section_graph = null
var app = null

func _ready():
	GUIUtils.populate_menus(MENU, self)
	section_graph = find_parent('SectionGraph')

func edit():
	# Hide menu popup to avoid problems when showing and hiding elements
	$MenuButton.get_popup().hide()
	$OptionText.hide()
	$LineEdit.show()
	$LineEdit.grab_focus()
	$LineEdit.text = $OptionText.text
	$LineEdit.caret_position = $LineEdit.text.length()

func save():
	$OptionText.text = $LineEdit.text
	$LineEdit.text = ""
	$LineEdit.hide()
	$OptionText.show()

func disconnect_option():
	var parent = get_parent()
	var slot_index = get_index() - parent.initial_child_count - 1
	section_graph.remove_connections(parent.name, slot_index)

func delete_confirm():
	GUIUtils.ask_for_confirmation(
		tr('Do you really want to delete this option?'),
		section_graph.app, 'delete', null, self)

func delete():
	var parent = get_parent()
	var slot_index = get_index() - parent.initial_child_count - 1
	section_graph.remove_connections(parent.name, slot_index)
	if get_index() < parent.get_child_count() - 1:
		# Reorder connections from index onwards
		section_graph.reorder_connections(parent.name, slot_index)
	parent.remove_child(self)
	queue_free()

func move_up():
	var parent = get_parent()
	var slot_index = get_index() - parent.initial_child_count - 1
	if slot_index > 0:
		# Find connection here and remove it
		var current_connection = section_graph.find_connection(parent.name, slot_index)
		if current_connection:
			section_graph.remove_connections(parent.name, slot_index)
		# Find connection in upper sibling and remove it
		var upper_connection = section_graph.find_connection(parent.name, slot_index - 1)
		if upper_connection:
			section_graph.remove_connections(parent.name, slot_index - 1)
		# Move elements
		parent.move_child(self, self.get_index() - 1)
		# Recreate connections
		if current_connection:
			section_graph.get_node('GraphEdit').connect_node(parent.name, slot_index - 1, current_connection.to, current_connection.to_port)
		if upper_connection:
			section_graph.get_node('GraphEdit').connect_node(parent.name, slot_index, upper_connection.to, upper_connection.to_port)

func move_down():
	var parent = get_parent()
	var slot_index = get_index() - parent.initial_child_count - 1
	if slot_index < parent.get_child_count() - parent.initial_child_count - 2:
		# Find connection here and remove it
		var current_connection = section_graph.find_connection(parent.name, slot_index)
		if current_connection:
			section_graph.remove_connections(parent.name, slot_index)
		# Find connection in down sibling and remove it
		var down_connection = section_graph.find_connection(parent.name, slot_index + 1)
		if down_connection:
			section_graph.remove_connections(parent.name, slot_index + 1)
		# Move elements
		parent.move_child(self, self.get_index() + 1)
		# Recreate connections
		if current_connection:
			section_graph.get_node('GraphEdit').connect_node(parent.name, slot_index + 1, current_connection.to, current_connection.to_port)
		if down_connection:
			section_graph.get_node('GraphEdit').connect_node(parent.name, slot_index, down_connection.to, down_connection.to_port)

func select_direct_connection_section():
	section_graph.app.select_section(self, 'set_direct_connection')

func remove_direct_connection_confirm():
	GUIUtils.ask_for_confirmation(
		tr('Do you really want to remove direct label connection %s for this option?') % direct_connection,
		section_graph.app, 'remove_direct_connection', null, self)

func set_direct_connection(section):
	direct_connection = section
	# Show direct connection indicator
	$DirectConnection.show()
	$DirectConnection.hint_tooltip = section

func remove_direct_connection():
	direct_connection = null
	# Hide direct connection indicator
	$DirectConnection.hide()
	$DirectConnection.hint_tooltip = ''

func show_set_condition_expression():
	section_graph.app.ask_for_expression(self, 'set_condition_expression')

func unset_condition_expression_confirm():
	GUIUtils.ask_for_confirmation(
		tr('Do you really want to remove the condition expression for this option?'),
		section_graph.app, 'unset_condition_expression', null, self)

func set_condition_expression(expression):
	condition_expression = expression
	# Show contidion indicator
	$Condition.show()
	$Condition.hint_tooltip = expression

func unset_condition_expression():
	condition_expression = null
	# Hide direct connection indicator
	$Condition.hide()
	$Condition.hint_tooltip = ''

# Events

func _on_LineEdit_focus_exited():
	if $LineEdit.text.strip_edges():
		save()

func _on_LineEdit_text_entered(new_text):
	save()
