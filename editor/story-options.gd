extends WindowDialog

var app = null

onready var title = find_node('TitleHBox').get_node('LineEdit')
onready var author = find_node('AuthorHBox').get_node('LineEdit')
onready var description = find_node('DescriptionHBox').get_node('TextEdit')
onready var ext_story_path = find_node('StoryPath').get_node('LineEdit')
onready var ext_project_path = find_node('ProjectPath').get_node('LineEdit')

func init(app_instance):
	app = app_instance
	update_form_values()

func update_form_values():
	title.text = app.STORY_CONFIG['title']
	author.text = app.STORY_CONFIG['author']
	description.text = app.STORY_CONFIG['description']
	ext_story_path.text = app.STORY_CONFIG['ext_story_path']
	ext_project_path.text = app.STORY_CONFIG['ext_project_path']
	
func update_story_config():
	app.STORY_CONFIG['title'] = title.text
	app.STORY_CONFIG['author'] = author.text
	app.STORY_CONFIG['description'] = description.text
	app.STORY_CONFIG['ext_story_path'] = ext_story_path.text
	app.STORY_CONFIG['ext_project_path'] = ext_project_path.text

func set_story_path(path):
	ext_story_path.text = path

func set_project_path(path):
	ext_project_path.text = path

func _on_CancelButton_pressed():
	hide()

func _on_SaveButton_pressed():
	update_story_config()
	hide()

func _on_StoryPathButton_pressed():
	if app.CURRENT_DIR == null:
		app.CURRENT_DIR = app.BASE_FILE_PATH
	GUIUtils.ask_for_path(app.CURRENT_DIR, app, 'dir', null, 'set_story_path', null, self)

func _on_ProjectPathButton_pressed():
	if app.CURRENT_DIR == null:
		app.CURRENT_DIR = app.BASE_FILE_PATH
	GUIUtils.ask_for_path(app.CURRENT_DIR, app, 'dir', null, 'set_project_path', null, self)
